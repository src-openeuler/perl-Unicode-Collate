%define mod_name Unicode-Collate
Name:            perl-%{mod_name}
Version:         1.31
Release:         4
Summary:         Unicode Collation Algorithm (UCA)
License:         (GPL-1.0-or-later OR Artistic-1.0-Perl) AND Unicode-DFS-2016
URL:             http://search.cpan.org/dist/%{mod_name}/
Source0:         https://cpan.metacpan.org/authors/id/S/SA/SADAHIRO/%{mod_name}-%{version}.tar.gz

BuildRequires:   gcc coreutils findutils make perl-devel perl-generators perl-interpreter
BuildRequires:   perl(Carp) perl(constant) perl(ExtUtils::MakeMaker) >= 6.76 perl(File::Spec)
BuildRequires:   perl(strict) perl(warnings) perl(base) perl(Unicode::Normalize) perl(XSLoader)
Requires:        perl(Unicode::Normalize)
Conflicts:       perl < 4:5.22.0-347

%description
The package is an algorithm defined in Unicode Technical Report #10, which defines a
customizable method to compare two strings. These comparisons can then be used to collate
or sort text in any writing system and language that can be represented with Unicode.

%package_help

%prep
%autosetup -n %{mod_name}-%{version}

rm Collate/Locale/*

%build
perl mklocale
mv Locale/*.pl Collate/Locale
mv Korean.pm Collate/CJK

perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 OPTIMIZE="$RPM_OPT_FLAGS"
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Unicode*

%files help
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.31-4
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Aug 8 2024 dongyuzhen <dongyuzhen@h-partners.com> - 1.31-3
- correct license information

* Tue Oct 25 2022 zhoushuiqing <zhoushuiqing2@huawei.com> - 1.31-2
- opitomize the specfile

* Mon Dec  6 2021 guozhaorui <guozhaorui1@huawei.com> - 1.31-1
- update version to 1.31

* Fri Jan 29 2021 liudabo <liudabo1@huawei.com> - 1.29-1
- upgrade version to 1.29

* Thu Dec 10 2020 shixuantong<shixuantong@huawei.com> - 1.27-2
- Type: NA
- ID: NA
- SUG: NA
- DESC:update source0

* Wed Jul 22 2020 dingyue<dingyue5@huawei.com> - 1.27-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix spec rule in openeuler

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 1.25-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix spec rule in openeuler

* Fri Sep 20 2019 chengquan<chengquan3@huawei.com> - 1.25-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revise requires in openeuler

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.25-1
- Package init
